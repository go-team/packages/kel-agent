module github.com/k0swe/kel-agent

go 1.15

require (
	github.com/adrg/xdg v0.4.0
	github.com/gorilla/websocket v1.5.0
	github.com/imdario/mergo v0.3.13
	github.com/k0swe/wsjtx-go/v4 v4.0.1
	github.com/rs/zerolog v1.27.0
	github.com/stretchr/testify v1.7.2
	golang.org/x/sys v0.0.0-20220610221304-9f5ed59c137d // indirect
	gopkg.in/yaml.v3 v3.0.1
)
